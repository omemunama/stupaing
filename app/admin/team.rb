ActiveAdmin.register Team do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

permit_params :unique_identifier, :name, :position, :avatar

form do |f|
    f.inputs "Team" do
      f.input :name
      f.input :position
      f.input :avatar
    end
    f.actions
  end

end
