class ContactsController < ApplicationController
  def index
  	@contacts = Contact.all
  end

  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)

    respond_to do |format|
      if @contact.save
        ContactMailer.contact_message(@contact)
        format.html { redirect_to url_for(:controller => :welcome, :action => :index), notice: 'Contact was send.' }
      else
        format.html { render action: 'new' }
      end
    end
  end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_params
      params.require(:contact).permit(:name, :email, :phone, :msg)
    end
end
