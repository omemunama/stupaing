class ContactMailer < ApplicationMailer
	default from: "user1@stupaing.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.contact_mailer.contact_message.subject
  #
  def contact_message(contact)
    @contact = contact
    mail to: "nurs@stupaing.com", subject: "Message from Stupaing Contact"
  end
end
