class ApplicationMailer < ActionMailer::Base
  default from: "user1@stupaing.com"
  layout 'mailer'
end
